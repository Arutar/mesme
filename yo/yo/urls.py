from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic import RedirectView

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'yo.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    (r'^$', RedirectView.as_view(url='/android/')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^android/', include('android.urls')),
)
