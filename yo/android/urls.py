from django.conf.urls import patterns, url

from android import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^users/add/$', views.add_user, name='add_user'),
    url(r'^users/check/$', views.check_users, name='check_users'),
    url(r'^chats/create/$', views.create_chat, name='create_chat'),
    url(r'^chats/$', views.chats, name='chats'),
    url(r'^chats/(?P<chat_id>\d+)/messages/$', views.chat_messages, name='chat_messages'),
    url(r'^chats/(?P<chat_id>\d+)/messages/send/$', views.chat_send_message, name='chat_send_message'),
    url(r'^chats/(?P<chat_id>\d+)/messages/seen/$', views.chat_message_seen, name='chat_message_seen'),
)