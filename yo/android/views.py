from IPython.kernel.zmq.session import msg_header
from django.http import HttpResponse
from django.http import Http404
from django.core.exceptions import PermissionDenied
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt

from models import User, Chat, ChatUser, ChatMessage

import urllib2
import json

# testing purposes to send notification without database interactions
# Compose and send message to the phone through GCM server
def index(request):
    msg_sender = get_object_or_404(User, phone='+75554443322')
    chat = get_object_or_404(Chat, id='7')

    users = []
    chat_users = ChatUser.objects.filter(chat=chat)
    for chat_user in chat_users:
        chat_user_regid = chat_user.user.regid
        users.append(chat_user_regid)

    chat_id = str(chat.id)
    data = {'registration_ids':users, 'collapse_key':chat_id, 'data':{'chat_id':chat_id, 'msg':'You are ...', 'phone':msg_sender.phone}}
    # data = {'dry_run':True, 'registration_ids':['111'], 'collapse_key':chat_id, 'data':{'chat_id':chat_id, 'msg':'You are ...', 'phone':user.phone}}
    headers = {'Content-Type':'application/json', 'Authorization':'key=AIzaSyAXvq9xSrrgc2UKO2ZtZi1dyoFFTn6FD64'}
    url = 'https://android.googleapis.com/gcm/send'
    request = urllib2.Request(url, data=json.dumps(data), headers=headers)
    response = urllib2.urlopen(request)
    return HttpResponse(response)

    # user = get_object_or_404(User, phone='+75554443322')
    # chat_id = '7'
    # data = {'registration_ids':[user.regid], 'collapse_key':chat_id, 'data':{'chat_id':chat_id, 'msg':'You are ...', 'phone':user.phone}}
    # headers = {'Content-Type':'application/json', 'Authorization':'key=AIzaSyAXvq9xSrrgc2UKO2ZtZi1dyoFFTn6FD64'}
    # url = 'https://android.googleapis.com/gcm/send'
    # request = urllib2.Request(url, data=json.dumps(data), headers=headers)
    # response = urllib2.urlopen(request)
    # return HttpResponse(response)

# Adds or updates current user using the phone number and provided registration ID
@csrf_exempt
def add_user(request):
    if request.method == 'POST':
        phone = request.POST.get('phone', '')
        regid = request.POST.get('regid', '')
        if phone != '' and regid != '':
            if not User.objects.filter(phone=phone).exists():
                user = User(phone=phone, regid=regid)
                user.save()
            else:
                user = get_object_or_404(User, phone=phone)
                user.regid = regid
                user.save()
            return HttpResponse('User added.')
        else:
            return HttpResponse('Not enough data.')
    else:
        raise Http404

# Checks if provided numbers are registered as application users and return true/false json array
@csrf_exempt
def check_users(request):
    if request.method == 'POST':
        phones = request.POST.get('phones', '')
        regid = request.POST.get('regid', '')
        if phones != '' and regid != '':
            if User.objects.filter(regid=regid).exists():
                # decode json data
                checkArray = json.loads(phones)

                # get result array
                result = User.objects.filter(phone__in=checkArray).values_list('phone', flat=True)

                # encode result array
                for idx in xrange(len(checkArray)):
                    if checkArray[idx] in result:
                        checkArray[idx] = True
                    else:
                        checkArray[idx] = False

                # return result array
                return HttpResponse(json.dumps(checkArray))
            else:
                raise PermissionDenied()

        else:
            raise PermissionDenied()
    else:
        raise Http404

# Creates user chat in the database for provided phone numbers
@csrf_exempt
def create_chat(request):
    if request.method == 'POST':
        phones = request.POST.get('phones', '')
        regid = request.POST.get('regid', '')
        if phones != '' and regid != '':
            if User.objects.filter(regid=regid).exists():
                chat_owner = User.objects.get(regid=regid)

                # decode json data
                user_phones = json.loads(phones)

                # get chat users
                chat_users = User.objects.filter(phone__in=user_phones)

                # create chat and add chat users
                chat = Chat()
                chat.save()

                chatUser = ChatUser(chat=chat, user=chat_owner, message_seen=0)
                chatUser.save()
                for user in chat_users:
                    chatUser = ChatUser(chat=chat, user=user, message_seen=0)
                    chatUser.save()

                # return result
                return HttpResponse(json.dumps('[OK]'))
            else:
                raise PermissionDenied()

        else:
            raise PermissionDenied()
    else:
        raise Http404

# Returns all new user chats with the ID greater than last_chat_id as json array.
@csrf_exempt
def chats(request):
    if request.method == 'POST':
        regid = request.POST.get('regid', '')
        chat_id = request.POST.get('last_chat_id', '')
        if regid != '' and chat_id != '':
            if User.objects.filter(regid=regid).exists():
                chat_owner = User.objects.get(regid=regid)

                # get all chats
                json_chats = []
                chats = ChatUser.objects.filter(user=chat_owner, chat__gt=chat_id)
                for chat in chats:
                    chat_users = ChatUser.objects.filter(chat=chat.chat).values_list('user', flat=True)
                    chat_users_phones = User.objects.filter(id__in=chat_users).values_list('phone', flat=True)
                    last_msg = ChatMessage.objects.filter(chat=chat.chat).order_by('-id').values_list('message', flat=True)
                    if len(last_msg) == 0:
                        last_msg = ''
                    else:
                        last_msg = last_msg[0]
                    json_chats.append({'chat_id':chat.chat.id, 'users':list(chat_users_phones), 'last_msg':last_msg})

                # return result array
                return HttpResponse(json.dumps(json_chats))
            else:
                raise PermissionDenied()

        else:
            raise PermissionDenied()
    else:
        raise Http404

# Returns all new chat messages that have ID greater than last_msg_id as json array
@csrf_exempt
def chat_messages(request, chat_id):
    if request.method == 'POST':
        regid = request.POST.get('regid', '')
        msg_id = request.POST.get('last_msg_id', '')
        if regid != '' and msg_id != '':
            if User.objects.filter(regid=regid).exists():
                chat = get_object_or_404(Chat, id=chat_id)

                # get all chat messages
                messages = []
                chat_messages = ChatMessage.objects.filter(chat=chat, id__gt=msg_id)
                for chat_message in chat_messages:
                    phone = chat_message.user.phone
                    message = chat_message.message
                    messages.append({'msg_id':chat_message.id, 'phone':phone, 'message':message})

                # return result array
                return HttpResponse(json.dumps(messages))
            else:
                raise PermissionDenied()

        else:
            raise PermissionDenied()
    else:
        raise Http404

# Stores message and sends it using the GCM server to all chat users.
@csrf_exempt
def chat_send_message(request, chat_id):
    if request.method == 'POST':
        msg_text = request.POST.get('msg', '')
        regid = request.POST.get('regid', '')
        if regid != '' and msg_text != '':
            if User.objects.filter(regid=regid).exists():
                msg_sender = User.objects.get(regid=regid)
                chat = get_object_or_404(Chat, id=chat_id)

                # save message
                msg = ChatMessage(chat = chat, user=msg_sender, message=msg_text)
                msg.save()

                # get chat users
                users = []
                chat_users = ChatUser.objects.filter(chat=chat)
                for chat_user in chat_users:
                    chat_user_regid = chat_user.user.regid
                    users.append(chat_user_regid)

                # send message to other chat users
                chat_id = str(chat.id)
                data = {'registration_ids':users, 'collapse_key':chat_id, 'data':{'chat_id':chat_id, 'msg':msg_text, 'phone':msg_sender.phone}}
                headers = {'Content-Type':'application/json', 'Authorization':'key=AIzaSyAXvq9xSrrgc2UKO2ZtZi1dyoFFTn6FD64'}
                url = 'https://android.googleapis.com/gcm/send'
                request = urllib2.Request(url, data=json.dumps(data), headers=headers)
                response = urllib2.urlopen(request)

                # return result array
                return HttpResponse(json.dumps('[OK]'))
            else:
                raise PermissionDenied()

        else:
            raise PermissionDenied()
    else:
        raise Http404

# Updates seen variable and sends it using the GCM server to all other chat users.
@csrf_exempt
def chat_message_seen(request, chat_id):
    if request.method == 'POST':
        msg_id = request.POST.get('msg_id', '')
        regid = request.POST.get('regid', '')
        if regid != '' and msg_id != '':
            if User.objects.filter(regid=regid).exists():
                msg_sender = get_object_or_404(User, regid=regid)
                chat = get_object_or_404(Chat, id=chat_id)

                # get message
                msg = get_object_or_404(ChatMessage, id=msg_id)

                # update seen variable, if changed notify users
                chat_user_update = get_object_or_404(ChatUser, chat=chat, user=msg_sender)
                if chat_user_update.message_seen < msg_id:
                    chat_user_update.message_seen = msg_id
                    chat_user_update.save()

                    # get chat users except the sender
                    users = []
                    chat_users = ChatUser.objects.filter(chat=chat).exclude(user=msg_sender)
                    for chat_user in chat_users:
                        chat_user_regid = chat_user.user.regid
                        users.append(chat_user_regid)

                    # send seen variable to other chat users
                    chat_id = str(chat.id)
                    collapse_key = chat_id + '_' + msg_id
                    data = {'registration_ids':users, 'collapse_key':collapse_key, 'data':{'chat_id':chat_id, 'msg_id':msg_id, 'phone':msg_sender.phone}}
                    headers = {'Content-Type':'application/json', 'Authorization':'key=AIzaSyAXvq9xSrrgc2UKO2ZtZi1dyoFFTn6FD64'}
                    url = 'https://android.googleapis.com/gcm/send'
                    request = urllib2.Request(url, data=json.dumps(data), headers=headers)
                    response = urllib2.urlopen(request)

                # return result array
                return HttpResponse(json.dumps('[OK]'))
            else:
                raise PermissionDenied()

        else:
            raise PermissionDenied()
    else:
        raise Http404

