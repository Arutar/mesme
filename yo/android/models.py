from django.db import models

class User(models.Model):
    id = models.AutoField(primary_key=True)
    phone = models.CharField(max_length=32)
    regid = models.CharField(max_length=256)

class Chat(models.Model):
    id = models.AutoField(primary_key=True)

class ChatUser(models.Model):
    chat = models.ForeignKey(Chat)
    user = models.ForeignKey(User)
    message_seen = models.IntegerField()

class ChatMessage(models.Model):
    chat = models.ForeignKey(Chat)
    user = models.ForeignKey(User)
    message = models.TextField()