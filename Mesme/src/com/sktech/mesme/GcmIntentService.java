package com.sktech.mesme;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.sktech.mesme.activity.ChatActivity;
import com.sktech.mesme.activity.ChatListActivity;
import com.sktech.mesme.activity.MainActivity;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.widget.RemoteViews;

/**
 * @author mikhail
 * IntentService for handling the server messages
 */
public class GcmIntentService extends IntentService {
    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;
    
    public static final String TAG = "Mesme";

    /**
     * Base constructor
     */
    public GcmIntentService() {
        super("GcmIntentService");
    }

    /**
     * Handles the Intent
     * <p>
     * Sends the OrderedBroadcast. If not handled creates a notification.
     * Updates the seen variable. If updated sends broadcast to update cursor.
     * @see android.app.IntentService#onHandleIntent(android.content.Intent)
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);

        if (!extras.isEmpty()) {
            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                sendNotification("Send error: " + extras.toString());
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
                sendNotification("Deleted messages on server: " + extras.toString());
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
            	if (extras.getString("msg", null) != null) {
	            	// create Intent with message info
	            	Intent messageInfoIntent = new Intent(MainActivity.NEW_MESSAGE_ACTION);
	            	messageInfoIntent.putExtras(extras);
	            	
	                // send ordered broadcast
	            	getApplicationContext().sendOrderedBroadcast(messageInfoIntent, null,
	        			new BroadcastReceiver() {
	        				@Override
	        				public void onReceive(Context context, Intent intent) {
	        					// check if message was served
	        					if (getResultCode() != MainActivity.MESSAGE_SERVED) {
	        						Bundle extras = intent.getExtras();
	        						
	        						final Intent restartChatActivtyIntent = new Intent(context,
	        								ChatActivity.class);
	        						restartChatActivtyIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	        						restartChatActivtyIntent.putExtra(ChatActivity.CHAT_ID,
	        								Long.parseLong(extras.getString("chat_id")));
	        						restartChatActivtyIntent.putExtra(ChatActivity.CHAT_TITLE,
	        								extras.getString("phone"));
	        						restartChatActivtyIntent.putExtra(ChatActivity.PHONE_NUMBER,
	        								extras.getString("phone"));
	        						PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
	        								restartChatActivtyIntent, PendingIntent.FLAG_UPDATE_CURRENT);
	        						
	        						RemoteViews mContentView = new RemoteViews(
	        								context.getPackageName(),
	        								R.layout.custom_notification);
	        						
	        						// get notification title
	        						String title = "";
	        						DatabaseOpenHelper dbHelper = new DatabaseOpenHelper(context);
	        						Cursor chat_title_cursor = dbHelper.getReadableDatabase().query(
	        								DatabaseOpenHelper.CHAT_TABLE_NAME,
	        								DatabaseOpenHelper.chat_columns,
	        								DatabaseOpenHelper.CHAT_ID + " = " + extras.getString("chat_id"),
	        								new String[] {}, null, null, null, null);
	        						if (chat_title_cursor.moveToNext()) {
	        							title = chat_title_cursor.getString(
	        									chat_title_cursor.getColumnIndex(DatabaseOpenHelper.CHAT_TITLE));
	        						}
	        	
	        						// set notification text and title
	        						mContentView.setTextViewText(R.id.notification_text, extras.getString("msg"));
	        						mContentView.setTextViewText(R.id.notification_chat_title, title);
	
	        						Notification.Builder notificationBuilder = new Notification.Builder(
	        								getApplicationContext())
	        						.setSmallIcon(R.drawable.ic_action_new_email)
	        						.setAutoCancel(true)
	        						.setContentIntent(pendingIntent)
	        						.setContent(mContentView);
	        	
	    							// Send the notification
	        						NotificationManager mNotificationmanager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE); 
	        						mNotificationmanager.notify(NOTIFICATION_ID, notificationBuilder.build());
	        					}
	        				}
	        			}, null, 0, null, null);
            	} else {
            		// update database seen variables
            		DatabaseOpenHelper mDbHelper = new DatabaseOpenHelper(getApplicationContext());
            		ContentValues values = new ContentValues();
            		values.put(DatabaseOpenHelper.MESSAGE_SEEN, "1");
            		mDbHelper.getWritableDatabase().update(DatabaseOpenHelper.MESSAGE_TABLE_NAME, values,
		    				DatabaseOpenHelper.MESSAGE_CHAT_ID + " = " + extras.getString("chat_id") + " AND " +
		    				DatabaseOpenHelper.MESSAGE_SEEN + " = 0" + " AND " +
		    				DatabaseOpenHelper.MESSAGE_ID + " <= " + extras.getString("msg_id"),
		    				new String[] {});
            		// send broadcast for cursor update
	            	Intent updateCursorIntent = new Intent(MainActivity.CURSOR_UPDATE_ACTION);
	            	getApplicationContext().sendBroadcast(updateCursorIntent);
            	}
            }
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    /**
     * Put the message into a notification and post it.
     * @param msg message text
     */
    private void sendNotification(String msg) {
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, ChatListActivity.class), 0);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
        .setSmallIcon(R.drawable.ic_action_warning)
        .setAutoCancel(true)
        .setContentIntent(pendingIntent)
        .setContentTitle("GCM Notification")
        .setContentText(msg)
        .setStyle(new NotificationCompat.BigTextStyle()
        .bigText(msg));

        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }
}
