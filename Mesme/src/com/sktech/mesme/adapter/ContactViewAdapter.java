package com.sktech.mesme.adapter;

import java.util.ArrayList;

import com.sktech.mesme.R;
import com.sktech.mesme.R.id;
import com.sktech.mesme.R.layout;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

/**
 * @author mikhail
 * ViewAdapter to store and display user contacts
 */
public class ContactViewAdapter extends BaseAdapter {
	static final String KEY = "file_path";
	private ArrayList<ContactInfoHolder> list = new ArrayList<ContactInfoHolder>();
	private static LayoutInflater inflater = null;
	private Context mContext;
	
	/**
	 * Base constructor
	 * @param context application context
	 */
	public ContactViewAdapter(Context context) {
		mContext = context;
		inflater = LayoutInflater.from(mContext);
	}

	/**
	 * Returns number of contacts
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() {
		return list.size();
	}

	/**
	 * Return user contact
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public Object getItem(int idx) {
		return list.get(idx);
	}
	
	/**
	 * Return user contact ID
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int idx) {
		return idx;
	}

	/**
	 * Creates and returns contact view.
	 * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getView(int idx, View contentView, ViewGroup parent) {
		View newView = contentView;
		ViewHolder holder;
		
		final ContactInfoHolder curr = list.get(idx);
		
		if (contentView == null) {
			holder = new ViewHolder();
			newView = inflater.inflate(R.layout.contact_view, null);
			holder.contact_selected = (CheckBox) newView.findViewById(R.id.contact_selected);
			holder.name = (TextView) newView.findViewById(R.id.name);
			holder.phone = (TextView) newView.findViewById(R.id.phone);
			newView.setTag(holder);
		} else {
			holder = (ViewHolder) newView.getTag();
		}
		
		holder.contact_selected.setChecked(curr.checked);
		holder.name.setText(curr.name);
		holder.phone.setText(curr.phone);
		
		holder.contact_selected.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				curr.checked = isChecked;
			}
		});
		
		return newView;
	}
	
	/**
	 * Returns ArrayList of contacts
	 * @return inner adapter list
	 */
	public ArrayList<ContactInfoHolder> getList(){
		return list;
	}
	
	/**
	 * Adds contact to the adapter
	 * @param item item to add
	 */
	public void add(ContactInfoHolder item) {
		list.add(item);
	}
	
	/**
	 * Remove all elements
	 */
	public void removeAll() {
		list.clear();
		notifyDataSetChanged();
	}
	
	/**
	 * @author mikhail
	 * Holder for contact view
	 */
	static class ViewHolder {
		CheckBox contact_selected;
		TextView name;
		TextView phone;
	}
	
	/**
	 * @author mikhail
	 * Holder for contact data
	 */
	public static class ContactInfoHolder {
		public boolean checked;
		public String name;
		public String phone;
	}
	
}
