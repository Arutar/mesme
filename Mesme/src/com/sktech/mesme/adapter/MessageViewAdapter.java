package com.sktech.mesme.adapter;

import java.io.FileNotFoundException;
import java.io.InputStream;

import com.sktech.mesme.DatabaseOpenHelper;
import com.sktech.mesme.R;
import com.sktech.mesme.activity.ChatActivity;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

/**
 * @author mikhail
 * ViewAdapter to display user chat messages
 */
public class MessageViewAdapter extends SimpleCursorAdapter {
	static final String PHONE_NUMBER = "phone_number";
	static final String CHAT_TITLE = "chat_title";
	static final String CHAT_ID = "chat_id";
	
	/**
	 * Base constructor
	 * @param context	The context where the ListView associated with this SimpleListItemFactory is running
	 * @param layout	resource identifier of a layout file that defines the views for this list item. The layout file should include at least those named views defined in "to"
	 * @param c			The database cursor. Can be null if the cursor is not available yet.
	 * @param from		A list of column names representing the data to bind to the UI. Can be null if the cursor is not available yet.
	 * @param to		The views that should display column in the "from" parameter. These should all be TextViews. The first N views in this list are given the values of the first N columns in the from parameter. Can be null if the cursor is not available yet.
	 * @param flags		Flags used to determine the behavior of the adapter, as per CursorAdapter(Context, Cursor, int).
	 */
	public MessageViewAdapter(Context context, int layout, Cursor c,
			String[] from, int[] to, int flags) {
		super(context, layout, c, from, to, flags);
	}
	
	/**
	 * Returns new view
	 * @see android.widget.ResourceCursorAdapter#newView(android.content.Context, android.database.Cursor, android.view.ViewGroup)
	 */
	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		return super.newView(context, cursor, parent);
	}
	
	/**
	 * Binds view. Sets gravity of the view so your messages will
	 * be displayed to the right and other messages will be
	 * displayed to the left.
	 * @see android.widget.SimpleCursorAdapter#bindView(android.view.View, android.content.Context, android.database.Cursor)
	 */
	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		super.bindView(view, context, cursor);
		
		LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.msg_linear);
		int seen = cursor.getInt(cursor.getColumnIndex(DatabaseOpenHelper.MESSAGE_SEEN));
		TextView textView = (TextView) linearLayout.findViewById(R.id.message_status);
		switch(seen) {
			case 0: textView.setText("sent");break;
			case 1: textView.setText("seen");break;
			default: textView.setText("seen");break;
		}
		
		String author = cursor.getString(cursor.getColumnIndex(DatabaseOpenHelper.MESSAGE_AUTHOR));
		if (author.equals("YOU")) {
			linearLayout.setGravity(Gravity.RIGHT);
			linearLayout.setPadding(50, 10, 10, 10);
			LinearLayout contentLinearLayout = (LinearLayout) view.findViewById(R.id.msg_content_linear);
			contentLinearLayout.setGravity(Gravity.RIGHT);
		} else {
			linearLayout.setGravity(Gravity.LEFT);
			linearLayout.setPadding(10, 10, 50, 10);
		}
	}
	
}
