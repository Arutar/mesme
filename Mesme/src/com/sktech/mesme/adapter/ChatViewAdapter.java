package com.sktech.mesme.adapter;

import java.io.FileNotFoundException;
import java.io.InputStream;

import com.sktech.mesme.DatabaseOpenHelper;
import com.sktech.mesme.R;
import com.sktech.mesme.activity.ChatActivity;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

/**
 * @author mikhail
 * ViewAdapter to store and display user chats
 */
public class ChatViewAdapter extends SimpleCursorAdapter {
	private final String TAG = "ChatViewAdapter";
	static final String PHONE_NUMBER = "phone_number";
	static final String CHAT_TITLE = "chat_title";
	static final String CHAT_ID = "chat_id";
	private Context mContext;
	private final ContentResolver mContentResolver;
	private final int mBitmapSize;
	private BitmapDrawable mNoPictureBitmap;
	
	/**
	 * Base constructor
	 * @param context	The context where the ListView associated with this SimpleListItemFactory is running
	 * @param layout	resource identifier of a layout file that defines the views for this list item. The layout file should include at least those named views defined in "to"
	 * @param c			The database cursor. Can be null if the cursor is not available yet.
	 * @param from		A list of column names representing the data to bind to the UI. Can be null if the cursor is not available yet.
	 * @param to		The views that should display column in the "from" parameter. These should all be TextViews. The first N views in this list are given the values of the first N columns in the from parameter. Can be null if the cursor is not available yet.
	 * @param flags		Flags used to determine the behavior of the adapter, as per CursorAdapter(Context, Cursor, int).
	 */
	public ChatViewAdapter(Context context, int layout, Cursor c,
			String[] from, int[] to, int flags) {
		super(context, layout, c, from, to, flags);
		mContext = context;
		mContentResolver = context.getContentResolver();
		
		// Default thumbnail bitmap for when contact has no thumbnail
		mNoPictureBitmap = (BitmapDrawable) context.getResources().getDrawable(
				R.drawable.ic_contact_picture);
		mBitmapSize = (int) context.getResources().getDimension(
				R.dimen.icon_height);
		mNoPictureBitmap.setBounds(0, 0, mBitmapSize, mBitmapSize);
	}
	
	/**
	 * Returns new view
	 * @see android.widget.ResourceCursorAdapter#newView(android.content.Context, android.database.Cursor, android.view.ViewGroup)
	 */
	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		return super.newView(context, cursor, parent);
	}
	
	/**
	 * Binds view. Sets OnClickListener for list item to start ChatActivity.
	 * @see android.widget.SimpleCursorAdapter#bindView(android.view.View, android.content.Context, android.database.Cursor)
	 */
	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		super.bindView(view, context, cursor);
		
		final String phone = cursor.getString(cursor.getColumnIndex(DatabaseOpenHelper.CHAT_PHONE));
		final String title = cursor.getString(cursor.getColumnIndex(DatabaseOpenHelper.CHAT_TITLE));
		final long chat_id = cursor.getInt(cursor.getColumnIndex(DatabaseOpenHelper.CHAT_ID));
		
		LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.chat_full);
		linearLayout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent chatIntent = new Intent(mContext, ChatActivity.class);
				chatIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				chatIntent.putExtra(PHONE_NUMBER, phone);
				chatIntent.putExtra(CHAT_TITLE, title);
				chatIntent.putExtra(CHAT_ID, chat_id);
				mContext.startActivity(chatIntent);
			}
		});
		
		// Get text view
		ImageView imageView = (ImageView) view.findViewById(R.id.chat_icon);

		// Default photo
		BitmapDrawable photoBitmap = mNoPictureBitmap;

		// Try to set actual thumbnail, if it's available
		String icon = cursor.getString(cursor.getColumnIndex(DatabaseOpenHelper.CHAT_ICON));

		if (!icon.equals("")) {

			InputStream input = null;

			try {

				// read thumbail data from memory
				input = mContentResolver.openInputStream(Uri
						.parse(icon));

				if (input != null) {
					photoBitmap = new BitmapDrawable(
							mContext.getApplicationContext().getResources(), input);
					photoBitmap.setBounds(0, 0, mBitmapSize, mBitmapSize);

				}
			} catch (FileNotFoundException e) {

				Log.i(TAG, "FileNotFoundException");

			}
		}

		imageView.setImageDrawable(photoBitmap);
	}
	
}
