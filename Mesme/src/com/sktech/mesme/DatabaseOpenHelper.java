package com.sktech.mesme;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * @author mikhail
 * Database helper. Helps to work with the application's database.
 * Creates tables for chats and messages. 
 */
public class DatabaseOpenHelper extends SQLiteOpenHelper {
	
	public final static String CHAT_TABLE_NAME = "chats";
	public final static String MESSAGE_TABLE_NAME = "messagess";
	public final static String CHAT_ID = "chat_id";
	public final static String CHAT_TITLE = "title";
	public final static String CHAT_ICON = "chat_icon";
	public final static String CHAT_PHONE = "phone";
	public final static String CHAT_LAST_MSG = "last_msg";
	public final static String MESSAGE_ID = "msg_id";
	public final static String MESSAGE_TEXT = "msg_text";
	public final static String MESSAGE_AUTHOR = "author";
	public final static String MESSAGE_CHAT_ID = "chat_id";
	public final static String MESSAGE_SEEN = "seen";
	public final static String _ID = "_id";
	public final static String[] chat_columns = { _ID, CHAT_ID, CHAT_TITLE, CHAT_PHONE, CHAT_LAST_MSG, CHAT_ICON};
	public final static String[] msg_columns = { _ID,  MESSAGE_ID, MESSAGE_TEXT, MESSAGE_AUTHOR, MESSAGE_CHAT_ID, MESSAGE_SEEN};

	final private static String CREATE_CHATS_CMD =

	"CREATE TABLE " + CHAT_TABLE_NAME + " (" 
			+ _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ CHAT_ID + " INTEGER UNIQUE NOT NULL, "
			+ CHAT_TITLE + " TEXT NOT NULL, "
			+ CHAT_ICON + " TEXT NOT NULL, "
			+ CHAT_PHONE + " VARCHAR(32) NOT NULL, "
			+ CHAT_LAST_MSG + " TEXT NOT NULL)";
	
	final private static String CREATE_MESSAGES_CMD =

			"CREATE TABLE " + MESSAGE_TABLE_NAME + " (" 
					+ _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
					+ MESSAGE_ID + " INTEGER UNIQUE NOT NULL, "
					+ MESSAGE_TEXT + " TEXT NOT NULL, "
					+ MESSAGE_AUTHOR + " TEXT NOT NULL, "
					+ MESSAGE_CHAT_ID + " INTEGER NOT NULL, "
					+ MESSAGE_SEEN + " INTEGER NOT NULL)";

	final private static String NAME = "mesme_db";
	final private static Integer VERSION = 1;
	final private Context mContext;

	/**
	 * Base constructor
	 * @param context	Application context. Will be used to delete this database.
	 */
	public DatabaseOpenHelper(Context context) {
		super(context, NAME, null, VERSION);
		this.mContext = context;
	}

	/**
	 * Creates chat and message tables.
	 * @see android.database.sqlite.SQLiteOpenHelper#onCreate(android.database.sqlite.SQLiteDatabase)
	 */
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_CHATS_CMD);
		db.execSQL(CREATE_MESSAGES_CMD);
	}

	/**
	 * Nothing.
	 * @see android.database.sqlite.SQLiteOpenHelper#onUpgrade(android.database.sqlite.SQLiteDatabase, int, int)
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// N/A
	}

	/**
	 * Deletes the database
	 */
	void deleteDatabase() {
		mContext.deleteDatabase(NAME);
	}
}