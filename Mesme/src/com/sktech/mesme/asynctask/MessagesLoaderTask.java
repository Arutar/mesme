package com.sktech.mesme.asynctask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.sktech.mesme.DatabaseOpenHelper;
import com.sktech.mesme.activity.MainActivity;
import com.sktech.mesme.adapter.ChatViewAdapter;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.widget.SimpleCursorAdapter;

/**
 * @author mikhail
 * AsyncTask for loading user chat messages from the server and storing them to the database. 
 */
public class MessagesLoaderTask extends ContactLoaderTask {
	private SimpleCursorAdapter mAdapter = null;
	private ChatViewAdapter mChatViewAdapter = null;
	private DatabaseOpenHelper mDbHelper;
	private String regid;
	private String mPhone;
	private long mChatId;

	/**
	 * Base constructor
	 * @param cr_				ContentResolver for getting users contacts
	 * @param adapter			ChatActivity adapter to store chat messages
	 * @param DbHelper			database helper for managing chat messages
	 * @param regid_			user registration ID
	 * @param chatId_			chat ID
	 * @param phone_			user phone number
	 * @param chatViewAdapter	ChatListActivity adapter to store last chat messages
	 */
	public MessagesLoaderTask(ContentResolver cr_, SimpleCursorAdapter adapter,
			DatabaseOpenHelper DbHelper, String regid_, long chatId_, String phone_,
			ChatViewAdapter chatViewAdapter) {
		super(cr_);
		
		mAdapter = adapter;
		mChatViewAdapter = chatViewAdapter;
		mDbHelper= DbHelper;
		regid = regid_;
		mPhone = phone_;
		mChatId = chatId_;
	}
	
	/**
	 * Loads all messages in background thread.
	 * @see com.sktech.mesme.asynctask.ContactLoaderTask#doInBackground(java.lang.Void[])
	 */
	@Override
	protected Void doInBackground(Void... path) {
		super.doInBackground(path);
		
		// Get messages
		getMessages();
		
		return null;
	}
	
	/**
	 * Gets new chat messages from the server.
	 * Updates server seen variable.
	 */
	private void getMessages() {
		// POST query to check, which of the contacts have application installed
	    HttpPost httppost = new HttpPost(MainActivity.SERVER_ADDRESS + "android/chats/" +
	    					Long.toString(mChatId) + "/messages/");
	    
	    try {
	    	HttpClient httpclient = new DefaultHttpClient();
	    	
	    	// Get last chat message id
	    	Cursor c = mDbHelper.getReadableDatabase().query(DatabaseOpenHelper.MESSAGE_TABLE_NAME,
					new String[] {DatabaseOpenHelper.MESSAGE_ID}, null, new String[] {}, null, null,
					DatabaseOpenHelper.MESSAGE_ID + " DESC", "1");
	    	String last_msg_id = "0";
	    	if(c.moveToNext()) {
	    		last_msg_id = c.getString(c.getColumnIndex(DatabaseOpenHelper.MESSAGE_ID));
	    	}
	    	
	        // Add your data
	        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
	        nameValuePairs.add(new BasicNameValuePair("regid", regid));
	        nameValuePairs.add(new BasicNameValuePair("last_msg_id", last_msg_id));
	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
	
	        // Execute HTTP Post Request
	        HttpResponse response = httpclient.execute(httppost);
	        
	        // Read response
	        BufferedReader reader = new BufferedReader(
	        		new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
	        String json = reader.readLine();
	        JSONTokener tokener = new JSONTokener(json);
	        JSONArray jsonMessages = null;
	        try {
	        	jsonMessages = new JSONArray(tokener);
	        	if (!jsonMessages.isNull(0)) {
	        		ContentValues values = new ContentValues();
		            for(int i = 0;i < jsonMessages.length();i++) {
		            	// get chat message data
		            	JSONObject jsonMessage = jsonMessages.getJSONObject(i);
		            	String messageText = jsonMessage.getString("message");
		            	long messageId = jsonMessage.getLong("msg_id");
		            	String messageAuthor = jsonMessage.getString("phone");
		            	String author = mContacts.get(messageAuthor);
		            	int seen = 0;
		            	if (messageAuthor.equals(mPhone)) {
		            		messageAuthor = "YOU";
		            	} else if (author != null) {
		            		messageAuthor = author;
		            		seen = 1;
		            	}
		            	
		            	// add chat message to database
		            	values.clear();
		        		values.put(DatabaseOpenHelper.MESSAGE_AUTHOR, messageAuthor);
		        		values.put(DatabaseOpenHelper.MESSAGE_ID, messageId);
		        		values.put(DatabaseOpenHelper.MESSAGE_TEXT, messageText);
		        		values.put(DatabaseOpenHelper.MESSAGE_CHAT_ID, Long.toString(mChatId));
		        		values.put(DatabaseOpenHelper.MESSAGE_SEEN, Integer.toString(seen));
		        		try {
			    			mDbHelper.getWritableDatabase().insertOrThrow(
			    					DatabaseOpenHelper.MESSAGE_TABLE_NAME, null, values);
			    		} catch (Exception e) {}
		            }
		            
		            // update server seen variable
		            if (jsonMessages.length() > 0) {
		            	JSONObject jsonMessage = jsonMessages.getJSONObject(jsonMessages.length() - 1);
		            	long messageId = jsonMessage.getLong("msg_id");
		            	httppost = new HttpPost(MainActivity.SERVER_ADDRESS + "android/chats/" +
		    					Long.toString(mChatId) + "/messages/seen/");
		            	nameValuePairs = new ArrayList<NameValuePair>(2);
		            	nameValuePairs.add(new BasicNameValuePair("regid", regid));
		            	nameValuePairs.add(new BasicNameValuePair("msg_id", Long.toString(messageId)));
		            	httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		            	response = httpclient.execute(httppost);
		            }
	            }
			} catch (JSONException e) {}
	
	    } catch (ClientProtocolException e) {
	    } catch (IOException e) {
	    }
	}
	
	/**
	 * Update adapter's data cursor. Updates last chat message in the database.
	 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
	 */
	@Override
	protected void onPostExecute(Void v) {
		// get cursor to chat last message
		Cursor last_msg_cursor = mDbHelper.getReadableDatabase().query(
				DatabaseOpenHelper.MESSAGE_TABLE_NAME,
				DatabaseOpenHelper.msg_columns,
				DatabaseOpenHelper.MESSAGE_CHAT_ID + " = " + Long.toString(mChatId),
				new String[] {}, null, null, DatabaseOpenHelper.MESSAGE_ID + " DESC", "1");
		
		// update last message
		if (last_msg_cursor.moveToNext()) {
			ContentValues updateValues = new ContentValues();
			updateValues.put(DatabaseOpenHelper.CHAT_LAST_MSG,
					last_msg_cursor.getString(last_msg_cursor.getColumnIndex(
							DatabaseOpenHelper.MESSAGE_TEXT)));
			mDbHelper.getWritableDatabase().update(DatabaseOpenHelper.CHAT_TABLE_NAME,
					updateValues, DatabaseOpenHelper.CHAT_ID + " = " + Long.toString(mChatId),
					new String[] {});
		}
		
		if(mChatViewAdapter != null) {
			// get cursor
			Cursor c = mDbHelper.getReadableDatabase().query(DatabaseOpenHelper.CHAT_TABLE_NAME,
					DatabaseOpenHelper.chat_columns,
					null, new String[] {}, null, null, null);
			
			// change adapter cursor
			mChatViewAdapter.changeCursor(c);
		}
		
		if(mAdapter != null) {
			// get cursor
			Cursor c = mDbHelper.getReadableDatabase().query(DatabaseOpenHelper.MESSAGE_TABLE_NAME,
					DatabaseOpenHelper.msg_columns,
					DatabaseOpenHelper.MESSAGE_CHAT_ID + " = " + Long.toString(mChatId),
					new String[] {}, null, null, DatabaseOpenHelper.MESSAGE_ID + " ASC");
			
			// change adapter cursor
			mAdapter.changeCursor(c);
		}
	}

}