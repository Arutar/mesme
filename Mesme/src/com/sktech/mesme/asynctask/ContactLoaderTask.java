package com.sktech.mesme.asynctask;

import java.util.HashMap;
import java.util.Map;

import android.content.ContentResolver;
import android.database.Cursor;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;

/**
 * @author mikhail
 * AsyncTask for loading user contacts.
 */
public class ContactLoaderTask extends AsyncTask<Void, Void, Void> {
	protected ContentResolver cr;
	protected Map<String, String> mContacts;
	protected Map<String, String> mContactThumbnails;
	
	/**
	 * Base constructor
	 * @param cr_ ContentResolver for getting users contacts
	 */
	public ContactLoaderTask(ContentResolver cr_) {
		super();
		
		cr = cr_;
		mContacts = new HashMap<String, String>();
		mContactThumbnails = new HashMap<String, String>();
	}

	/**
	 * Loads user contacts in background thread.
	 * @see android.os.AsyncTask#doInBackground(Params[])
	 */
	@Override
	protected Void doInBackground(Void... path) {
		
		// Obtain contact names and numbers
		getContacts();
		
		return null;
	}
	
	/**
	 * Gets all the user contacts with their phone numbers
	 */
	private void getContacts() {
		Cursor c = cr.query(ContactsContract.Contacts.CONTENT_URI, 
				new String[] {ContactsContract.Contacts._ID,
				ContactsContract.Contacts.DISPLAY_NAME,
				ContactsContract.Contacts.PHOTO_THUMBNAIL_URI},
					null, null, null);
		
		if (c.moveToFirst()) {
			do {
				String name;
				String phone = "";
				name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
				String contactId =
				        c.getString(c.getColumnIndex(ContactsContract.Contacts._ID));
				String contactThumbnail =
				        c.getString(c.getColumnIndex(ContactsContract.Contacts.PHOTO_THUMBNAIL_URI));
				Cursor phones = cr.query(Phone.CONTENT_URI, null,
				        Phone.CONTACT_ID + " = " + contactId, null, null);
				while (phones.moveToNext()) {
					phone = phones.getString(phones.getColumnIndex(Phone.NUMBER));
			        if (phones.getInt(phones.getColumnIndex(Phone.IS_SUPER_PRIMARY)) != 0) {
			        	break;
			        }
			    }
			    phones.close();
			    if (phone != "") {
			    	mContacts.put(phone, name);
			    	mContactThumbnails.put(phone, contactThumbnail);
			    }
			} while (c.moveToNext());
		}
		c.close();
	}

}