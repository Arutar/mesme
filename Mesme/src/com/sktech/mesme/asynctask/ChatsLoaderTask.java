package com.sktech.mesme.asynctask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;

import com.sktech.mesme.DatabaseOpenHelper;
import com.sktech.mesme.activity.MainActivity;
import com.sktech.mesme.adapter.ChatViewAdapter;

/**
 * @author mikhail
 * AsyncTask that loads user chats from the server and stores them in the application's database.
 */
public class ChatsLoaderTask extends ContactLoaderTask {
	private ChatViewAdapter mAdapter = null;
	private DatabaseOpenHelper mDbHelper;
	private String regid;
	private String mPhone;

	/**
	 * Base constructor
	 * @param cr_ 		ContentResolver for getting users contacts
	 * @param DbHelper	database helper for managing chats
	 * @param adapter	adapter to load chats into
	 * @param regid_	user registration ID
	 * @param phone		user phone number
	 */
	public ChatsLoaderTask(ContentResolver cr_, ChatViewAdapter adapter,
			DatabaseOpenHelper DbHelper, String regid_, String phone) {
		super(cr_);
		
		mAdapter = adapter;
		mDbHelper = DbHelper;
		regid = regid_;
		mPhone = phone;
	}
	
	/**
	 * Loads all user chats in background thread.
	 * @see com.sktech.mesme.asynctask.ContactLoaderTask#doInBackground(java.lang.Void[])
	 */
	@Override
	protected Void doInBackground(Void... path) {
		super.doInBackground(path);
		
		// Get chats
		getChats();
		
		return null;
	}
	
	/**
	 * Get new user chats from the server
	 */
	private void getChats() {
		// POST query to check, which of the contacts have application installed
	    HttpPost httppost = new HttpPost(MainActivity.SERVER_ADDRESS + "android/chats/");
	    
	    try {
	    	HttpClient httpclient = new DefaultHttpClient();
	    	
	    	// Get last chat id
	    	Cursor c = mDbHelper.getReadableDatabase().query(DatabaseOpenHelper.CHAT_TABLE_NAME,
					new String[] {DatabaseOpenHelper.CHAT_ID}, null, new String[] {}, null, null,
					DatabaseOpenHelper.CHAT_ID + " DESC", "1");
	    	String last_chat_id = "0";
	    	if(c.moveToNext()) {
	    		last_chat_id = c.getString(c.getColumnIndex(DatabaseOpenHelper.CHAT_ID));
	    	}
	    	
	        // Add your data
	        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
	        nameValuePairs.add(new BasicNameValuePair("regid", regid));
	        nameValuePairs.add(new BasicNameValuePair("last_chat_id", last_chat_id));
	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
	
	        // Execute HTTP Post Request
	        HttpResponse response = httpclient.execute(httppost);
	        
	        // Read response
	        BufferedReader reader = new BufferedReader(
	        		new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
	        String json = reader.readLine();
	        JSONTokener tokener = new JSONTokener(json);
	        JSONArray jsonChats = null;
	        try {
	        	jsonChats = new JSONArray(tokener);
	        	if (!jsonChats.isNull(0)) {
	        		ContentValues values = new ContentValues();
		            for(int i = 0;i < jsonChats.length();i++) {
		            	// get chat data
		            	JSONObject jsonChat = jsonChats.getJSONObject(i);
		            	long chat_id = jsonChat.getLong("chat_id");
		            	String lastMessage = jsonChat.getString("last_msg");
		            	JSONArray jsonPhones = jsonChat.getJSONArray("users");
		            	String phone = jsonPhones.getString(0);
		            	for(int j = 1;j < jsonPhones.length();j++) {
		            		if (!mPhone.equals(jsonPhones.getString(j))) {
		            			phone = jsonPhones.getString(j);
		            			break;
		            		}
		            	}
		            	String title = mContacts.get(phone);
		            	if (title == null) {
		            		title = phone;
		            	}
		            	String icon = mContactThumbnails.get(phone);
		            	if (icon == null) {
		            		icon = "";
		            	}
		            	
		            	// add chat to database
		            	values.clear();
		        		values.put(DatabaseOpenHelper.CHAT_ID, chat_id);
		        		values.put(DatabaseOpenHelper.CHAT_TITLE, title);
		        		values.put(DatabaseOpenHelper.CHAT_ICON, icon);
		        		values.put(DatabaseOpenHelper.CHAT_PHONE, phone);
		        		values.put(DatabaseOpenHelper.CHAT_LAST_MSG, lastMessage);
		        		try {
			    			mDbHelper.getWritableDatabase().insertOrThrow(
			    					DatabaseOpenHelper.CHAT_TABLE_NAME, null, values);
			    		} catch (Exception e) {}
		            }
	            }
			} catch (JSONException e) {}
	
	    } catch (ClientProtocolException e) {
	    } catch (IOException e) {
	    }
	}
	
	/**
	 * Update adapter's data cursor
	 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
	 */
	@Override
	protected void onPostExecute(Void v) {
		// get cursor
		Cursor c = mDbHelper.getReadableDatabase().query(DatabaseOpenHelper.CHAT_TABLE_NAME,
				DatabaseOpenHelper.chat_columns,
				null, new String[] {}, null, null, null);
		
		// change adapter cursor
		mAdapter.changeCursor(c);
	}

}