package com.sktech.mesme.asynctask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONTokener;

import com.sktech.mesme.activity.MainActivity;
import com.sktech.mesme.adapter.ContactViewAdapter;
import com.sktech.mesme.adapter.ContactViewAdapter.ContactInfoHolder;

import android.content.ContentResolver;

/**
 * @author mikhail
 * Load all user contacts into adapter asynchronously
 */
public class PickContactsLoaderTask extends ContactLoaderTask {
	private ContactViewAdapter mAdapter = null;
	private String regid;
	Map<String, Boolean> mApplicationInstalled = new HashMap<String, Boolean>();

	/**
	 * Base constructor
	 * @param cr_		ContentResolver for getting users contacts
	 * @param adapter	adapter to store contacts
	 * @param regid_	user registration ID
	 */
	public PickContactsLoaderTask(ContentResolver cr_, ContactViewAdapter adapter, String regid_) {
		super(cr_);
		
		mAdapter = adapter;
		regid = regid_;
	}
	
	/**
	 * Loads all contacts and check which of them have application installed.
	 * @see com.sktech.mesme.asynctask.ContactLoaderTask#doInBackground(java.lang.Void[])
	 */
	@Override
	protected Void doInBackground(Void... path) {
		super.doInBackground(path);
		
		// Check which of the contacts have application installed
		isApplicationInstalled();
		
		return null;
	}
	
	/**
	 * Checks which of the loaded contacts have
	 * application installed.
	 */
	private void isApplicationInstalled() {
		// POST query to check, which of the contacts have application installed
	    HttpPost httppost = new HttpPost(MainActivity.SERVER_ADDRESS + "android/users/check/");
	    // store keys to guarantee the same order
	    ArrayList<String> keys = new ArrayList<String>(mContacts.keySet());
	    JSONArray phoneNumbers = new JSONArray();
	    for(int i = 0;i < keys.size();i++) {
	    	phoneNumbers.put(keys.get(i));
	    }
	    
	    try {
	    	HttpClient httpclient = new DefaultHttpClient();
	    	
	        // Add your data
	        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
	        nameValuePairs.add(new BasicNameValuePair("regid", regid));
	        nameValuePairs.add(new BasicNameValuePair("phones", phoneNumbers.toString()));
	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
	
	        // Execute HTTP Post Request
	        HttpResponse response = httpclient.execute(httppost);
	        
	        // Read response
	        BufferedReader reader = new BufferedReader(
	        		new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
	        String json = reader.readLine();
	        JSONTokener tokener = new JSONTokener(json);
	        JSONArray jsonResponse = null;
	        try {
	        	jsonResponse = new JSONArray(tokener);
	        	if (jsonResponse != null) {
		            for(int i = 0;i < jsonResponse.length();i++) {
		            	if (jsonResponse.get(i).toString() == "true") {
		            		mApplicationInstalled.put(keys.get(i), true);
		            	} else if (jsonResponse.get(i).toString() == "false") {
		            		mApplicationInstalled.put(keys.get(i), false);
		            	} else { // should never happen
		            		mApplicationInstalled.put(keys.get(i), false);
		            	}
		            }
	            }
			} catch (JSONException e) {}
	
	    } catch (ClientProtocolException e) {
	    } catch (IOException e) {
	    }
	}

	/**
	 * Adds contacts to the adapter
	 */
	private void addContacts() {
		for(String key : mContacts.keySet()) {
			if (mApplicationInstalled.get(key)) {
				ContactInfoHolder cih = new ContactInfoHolder();
				cih.checked = false;
				cih.name = mContacts.get(key);
				cih.phone = key;
				mAdapter.add(cih);
			}
		}
	}
	
	/**
	 * Adds contacts to the adapter and notifies the observers.
	 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
	 */
	@Override
	protected void onPostExecute(Void v) {
		// Add contacts with application installed
		addContacts();
		mAdapter.notifyDataSetChanged();
	}

}
