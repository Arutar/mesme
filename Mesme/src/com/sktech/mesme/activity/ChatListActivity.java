package com.sktech.mesme.activity;

import com.sktech.mesme.DatabaseOpenHelper;
import com.sktech.mesme.R;
import com.sktech.mesme.adapter.ChatViewAdapter;
import com.sktech.mesme.asynctask.ChatsLoaderTask;
import com.sktech.mesme.asynctask.MessagesLoaderTask;

import android.app.ListActivity;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

/**
 * @author mikhail
 * Activity for displaying a list of chats.
 */
public class ChatListActivity extends ListActivity {
	private ChatViewAdapter mAdapter;
	private BroadcastReceiver mNewMessageReceiver;
	private DatabaseOpenHelper mDbHelper;
	
	String regid;
	String mPhone;

	/**
	 * Loads user preferences. Creates BroadcastReceiver for receiving new messages.
	 * Create adapter for chats. Display chats and their last messages.
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		getUserPrefs();
		
		createReceiver();
		
		getListView().setBackgroundColor(getResources().getColor(R.color.white));
		
		mDbHelper = new DatabaseOpenHelper(this);
		
		// create adapter
		mAdapter = new ChatViewAdapter(this, R.layout.chat_view, null,
				new String[] {	DatabaseOpenHelper.CHAT_TITLE,
								DatabaseOpenHelper.CHAT_LAST_MSG},
				new int[] { R.id.chat_title, R.id.chat_last_message }, 0);
		
		// get data
		updateCursor();
		
		setListAdapter(mAdapter);
	}

	/**
	 * Updates adapter's data cursor
	 */
	private void updateCursor() {
		// get chat data from database
		Cursor c = mDbHelper.getReadableDatabase().query(DatabaseOpenHelper.CHAT_TABLE_NAME,
				DatabaseOpenHelper.chat_columns,
				null, new String[] {}, null, null, null);
		
		mAdapter.changeCursor(c);
	}

	/**
	 * Creates BroadcastReceiver for receiving new message.
	 * Add message to the database. Display it as chat's last message.
	 */
	private void createReceiver() {
		mNewMessageReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				if (isOrderedBroadcast())
				{
					long chat_id = Long.parseLong(intent.getStringExtra("chat_id"));
					
					// add all new chat messages to database
					new MessagesLoaderTask(getContentResolver(), null,
							mDbHelper, regid, chat_id, mPhone, mAdapter).execute();
					
					setResultCode(MainActivity.MESSAGE_SERVED);
				}
			}
		};
	}

	/**
	 * Loads all chats asynchronously. Registers BroadcastReceiver
	 * for new messages.
	 * @see android.app.Activity#onResume()
	 */
	@Override
	public void onResume() {
		super.onResume();
		new ChatsLoaderTask(getContentResolver(), mAdapter, mDbHelper, regid, mPhone).execute();
		IntentFilter intentFilter = new IntentFilter(MainActivity.NEW_MESSAGE_ACTION);
		registerReceiver(mNewMessageReceiver, intentFilter);
	}
	
	/**
	 * Deletes all chats. Unregisters BroadcastReceiver
	 * for new messages.
	 * @see android.app.Activity#onPause()
	 */
	@Override
	public void onPause() {
		super.onPause();
		mAdapter.changeCursor(null);
		if (mNewMessageReceiver != null)
        {
        	unregisterReceiver(mNewMessageReceiver);
        }
		mDbHelper.close();
	}
	
	/**
	 * Get user phone number and registration ID.
	 */
	private void getUserPrefs() {
		SharedPreferences prefs = getSharedPreferences(MainActivity.class.getSimpleName(),
	            Context.MODE_PRIVATE);
		regid = prefs.getString(MainActivity.PROPERTY_REG_ID, "");
		mPhone = prefs.getString(MainActivity.PROPERTY_PHONE_NUMBER, "");
	}

	/**
	 * Creates menu.
	 * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.chat_list, menu);
		return true;
	}

	/**
	 * Starts CreateChatActivity on button click. Deletes database
	 * and closes application on button click.
	 * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_create_chat) {
			Intent createChatIntent = new Intent(this, CreateChatActivity.class);
			startActivity(createChatIntent);
			return true;
		}
		if (id == R.id.action_delete_database) {
			deleteDatabase("mesme_db");
			finish();
		}
		return super.onOptionsItemSelected(item);
	}
}
