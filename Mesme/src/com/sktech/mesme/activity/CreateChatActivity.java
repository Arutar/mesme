package com.sktech.mesme.activity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.sktech.mesme.R;
import com.sktech.mesme.R.id;
import com.sktech.mesme.R.menu;
import com.sktech.mesme.adapter.ContactViewAdapter;
import com.sktech.mesme.adapter.ContactViewAdapter.ContactInfoHolder;
import com.sktech.mesme.asynctask.PickContactsLoaderTask;

import android.app.ListActivity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

/**
 * @author mikhail
 * Activity for chat creation
 */
public class CreateChatActivity extends ListActivity {
	private static final String TAG = "Mesme";
	private ContactViewAdapter mAdapter;
	
	String regid;

	/**
	 * Load user preferences. Set up the contacts adapter.
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		getUserPrefs();
		
		mAdapter = new ContactViewAdapter(getApplicationContext());
		setListAdapter(mAdapter);
		
		new PickContactsLoaderTask(getContentResolver(), mAdapter, regid).execute();
	}
	
	/**
	 * Load user phone number and registration ID.
	 */
	private void getUserPrefs() {
		SharedPreferences prefs = getSharedPreferences(MainActivity.class.getSimpleName(),
	            Context.MODE_PRIVATE);
		regid = prefs.getString(MainActivity.PROPERTY_REG_ID, "");
	}

	/**
	 * Creates menu.
	 * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.create_chat, menu);
		return true;
	}

	/**
	 * Creates chat on menu button click
	 * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_done) {
			// POST data to the server to create chat
			createChat();
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	/**
	 * Send request to the message server to create chat.
	 */
	private void createChat() {
		new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
            	HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(MainActivity.SERVER_ADDRESS + "android/chats/create/");
                
                ArrayList<ContactInfoHolder> list = mAdapter.getList();
        		JSONArray jarray = new JSONArray();
        		for(int i = 0;i < list.size();i++) {
        			if (list.get(i).checked) {
        				jarray.put(list.get(i).phone);
        			}
        		}
                
                try {
                    // Add your data
                    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                    nameValuePairs.add(new BasicNameValuePair("regid", regid));
                    nameValuePairs.add(new BasicNameValuePair("phones", jarray.toString()));
                    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                    // Execute HTTP Post Request
                    HttpResponse response = httpclient.execute(httppost);
                } catch (ClientProtocolException e) {
                } catch (IOException e) {
                }
            	
                return null;
            }

            @Override
            protected void onPostExecute(Void msg) {
            }
        }.execute(null, null, null);
	}
}
