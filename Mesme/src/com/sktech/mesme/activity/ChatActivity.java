package com.sktech.mesme.activity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import com.sktech.mesme.DatabaseOpenHelper;
import com.sktech.mesme.R;
import com.sktech.mesme.adapter.MessageViewAdapter;
import com.sktech.mesme.asynctask.ContactLoaderTask;
import com.sktech.mesme.asynctask.MessagesLoaderTask;

import android.app.Activity;
import android.app.ListActivity;
import android.app.ListFragment;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

/**
 * @author mikhail
 * Activity for displaying chat messages
 */
public class ChatActivity extends Activity {
	public static final String PHONE_NUMBER = "phone_number";
	public static final String CHAT_TITLE = "chat_title";
	public static final String CHAT_ID = "chat_id";
	
	private SimpleCursorAdapter mAdapter;
	private DatabaseOpenHelper mDbHelper;
	private ListView mListView;
	private EditText mEditText;
	private BroadcastReceiver mNewMessageReceiver;
	private BroadcastReceiver mUpdateCursorReceiver;
	
	String regid;
	long chatId;
	String chatTitle;
	String mPhone;
	String phone;
	

	/**
	 * Gets chat ID, chat title, user phone number from the Intent. Get user preferences.
	 * Displays all chat messages.
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_chat);
		
		chatId = getIntent().getLongExtra(CHAT_ID, 0);
		chatTitle = getIntent().getStringExtra(CHAT_TITLE);
		phone = getIntent().getStringExtra(PHONE_NUMBER);
		
		mListView = (ListView) findViewById(R.id.msg_list);
		mEditText = (EditText) findViewById(R.id.msg_edit);
		
		getUserPrefs();
		
		setOnClickListener();
		
		createReceiver();
		
		mDbHelper = new DatabaseOpenHelper(this);
		
		// create adapter
		mAdapter = new MessageViewAdapter(this, R.layout.message_view, null,
				new String[] {	DatabaseOpenHelper.MESSAGE_AUTHOR,
								DatabaseOpenHelper.MESSAGE_TEXT},
				new int[] { R.id.message_author, R.id.message }, 0);
		
		// get data
		updateCursor();
		
		mListView.setAdapter(mAdapter);
		mListView.setSelection(mListView.getCount() - 1);
	}
	
	/**
	 * Updates adapter's data cursor
	 */
	private void updateCursor() {
		// get chat data from database
		Cursor c = mDbHelper.getReadableDatabase().query(DatabaseOpenHelper.MESSAGE_TABLE_NAME,
				DatabaseOpenHelper.msg_columns,
				DatabaseOpenHelper.MESSAGE_CHAT_ID + " = " + Long.toString(chatId),
				new String[] {}, null, null, DatabaseOpenHelper.MESSAGE_ID + " ASC");
		
		mAdapter.changeCursor(c);
	}

	/**
	 * Creates a BroadcastReceiver for receiving a new message
	 * and displaying it on the screen. Add message to the database.
	 * Creates a BroadcastReceiver for updating cursor
	 * after seen variable was updated.
	 */
	private void createReceiver() {
		mNewMessageReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				if (isOrderedBroadcast())
				{
					long chat_id = Long.parseLong(intent.getStringExtra("chat_id"));
					
					// add all new chat messages to database
					new MessagesLoaderTask(getContentResolver(), mAdapter,
							mDbHelper, regid, chat_id, mPhone, null).execute();
					
					setResultCode(MainActivity.MESSAGE_SERVED);
				}
			}
		};
		
		mUpdateCursorReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				updateCursor();
			}
		};
	}
	
	/**
	 * Loads of the messages asynchronously and registers the BroadcastReceiver
	 * for receiving new messages. Registers the BroadcastReceiver
	 * for updating cursor when seen variable is changed.
	 * @see android.app.Activity#onResume()
	 */
	@Override
	public void onResume() {
		super.onResume();
		new MessagesLoaderTask(getContentResolver(), mAdapter, mDbHelper,
				regid, chatId, mPhone, null).execute();
		IntentFilter intentFilter = new IntentFilter(MainActivity.NEW_MESSAGE_ACTION);
		registerReceiver(mNewMessageReceiver, intentFilter);
		intentFilter = new IntentFilter(MainActivity.CURSOR_UPDATE_ACTION);
		registerReceiver(mUpdateCursorReceiver, intentFilter);
	}
	
	/**
	 * Removes the messages and unregisters the BroadcastReceiver
	 * for receiving new messages. Unregisters the BroadcastReceiver
	 * for updating cursor when seen variable is changed.
	 * @see android.app.Activity#onPause()
	 */
	@Override
	public void onPause() {
		super.onPause();
		mAdapter.changeCursor(null);
		if (mNewMessageReceiver != null)
        {
        	unregisterReceiver(mNewMessageReceiver);
        }
		if (mUpdateCursorReceiver != null)
        {
        	unregisterReceiver(mUpdateCursorReceiver);
        }
		mDbHelper.close();
	}
	
	/**
	 * Sets OnClickListener for send button.
	 * Listener creates an AsyncTask to send the message
	 * to the server.
	 */
	private void setOnClickListener() {
		ImageButton imageButton = (ImageButton) findViewById(R.id.send_btn);
		imageButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				final String msg = mEditText.getText().toString();
				if (!msg.isEmpty()) {
					// send message task
					new AsyncTask<Void, Void, Void>() {
			            @Override
			            protected Void doInBackground(Void... params) {
			            	HttpPost httppost = new HttpPost(MainActivity.SERVER_ADDRESS + "android/chats/"+
			            			Long.toString(chatId)+"/messages/send/");
			        	    try {
			        	    	HttpClient httpclient = new DefaultHttpClient();
			        	    	
			        	        // Add your data
			        	        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			        	        nameValuePairs.add(new BasicNameValuePair("regid", regid));
			        	        nameValuePairs.add(new BasicNameValuePair("msg", msg));
			        	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			        	
			        	        // Execute HTTP Post Request
			        	        HttpResponse response = httpclient.execute(httppost);
			        	    } catch (ClientProtocolException e) {
			        	    } catch (IOException e) {
			        	    }
			            	
			                return null;
			            }
	
			            @Override
			            protected void onPostExecute(Void res) {
			            }
			        }.execute(null, null, null);
			        
			        mEditText.setText("");
				}
			}
		});
	}

	/**
	 * Load user phone number and registration ID.
	 */
	private void getUserPrefs() {
		SharedPreferences prefs = getSharedPreferences(MainActivity.class.getSimpleName(),
	            Context.MODE_PRIVATE);
		regid = prefs.getString(MainActivity.PROPERTY_REG_ID, "");
		mPhone = prefs.getString(MainActivity.PROPERTY_PHONE_NUMBER, "");
	}

	/**
	 * Creates menu
	 * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.chat, menu);
		return true;
	}

	/**
	 * Create menu button action
	 * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
