package com.sktech.mesme.activity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.sktech.mesme.R;
import com.sktech.mesme.R.id;
import com.sktech.mesme.R.layout;

/**
 * @author mikhail
 * Login Activity for getting registration ID and users phone number.
 */
public class MainActivity extends Activity {
	public static final String SERVER_ADDRESS = "http://192.168.1.33:8000/";
//	public static final String SERVER_ADDRESS = "http://10.18.10.186:8000/";
	public static final String EXTRA_MESSAGE = "message";
	public static final String PROPERTY_PHONE_NUMBER = "phone_number_id";
    public static final String PROPERTY_REG_ID = "registration_id";
    public static final String NEW_MESSAGE_ACTION = "com.sktech.mesme.NEW_MESSAGE";
    public static final String CURSOR_UPDATE_ACTION = "com.sktech.mesme.CURSOR_UPDATE";
    public static final int MESSAGE_SERVED = Activity.RESULT_FIRST_USER;
    private static final String PROPERTY_APP_VERSION = "appVersion";
	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	private static final String TAG = "Mesme";
	
	String SENDER_ID = "748122736332";
	
	GoogleCloudMessaging gcm;
    AtomicInteger msgId = new AtomicInteger();
    SharedPreferences prefs;
    Context context;
    
    String regid;
    String mPhoneNumber;

	/**
	 * Create activity. Get the registration ID from
	 * Google Cloud Messaging server. Send registration ID to the
	 * message server. Open ChatListActivity when registered.
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		context = getApplicationContext();
		if (!getRegistrationId(context).isEmpty()) {
			chatListActivity();
			finish();
		}
		
		Button button = (Button) findViewById(R.id.create_account_btn);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (checkPlayServices()) {
					// Do the work
					gcm = GoogleCloudMessaging.getInstance(MainActivity.this);
					regid = getRegistrationId(context);
					
					if (regid.isEmpty()) {
		                registerInBackground();
		            } else {
						// Open ChatList activity
						chatListActivity();
						finish();
		            }
				} else {
					Log.i(TAG, "No valid Google Play Services APK found.");
				}
			}
		});
	}
	
	/**
	 * Check for checkPlayServices. Try to resolve issues.
	 * @see android.app.Activity#onResume()
	 */
	@Override
	protected void onResume() {
		super.onResume();
		
		// check if GooglePlayServeces are available
		if (checkPlayServices()) {
			// Do the work
		} else {
			Toast.makeText(this, "You have no GooglePlayServices", Toast.LENGTH_SHORT).show();
		}
	}
	
	/**
	 * Starts ChatListActivity
	 */
	private void chatListActivity() {
		Intent chatListIntent = new Intent(MainActivity.this, ChatListActivity.class);
		startActivity(chatListIntent);
	}
	
	/**
	 * Check the device to make sure it has the Google Play Services APK. If
	 * it doesn't, display a dialog that allows users to download the APK from
	 * the Google Play Store or enable it in the device's system settings.
	 */
	private boolean checkPlayServices() {
	    int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
	    if (resultCode != ConnectionResult.SUCCESS) {
	        if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
	            GooglePlayServicesUtil.getErrorDialog(resultCode, this,
	                    PLAY_SERVICES_RESOLUTION_REQUEST).show();
	        } else {
	            Log.i(TAG, "This device is not supported.");
	            finish();
	        }
	        return false;
	    }
	    return true;
	}
	
	/**
	 * Gets the current registration ID for application on GCM service.
	 * <p>
	 * If result is empty, the app needs to register.
	 *
	 * @return registration ID, or empty string if there is no existing
	 *         registration ID.
	 */
	private String getRegistrationId(Context context) {
	    final SharedPreferences prefs = getGCMPreferences(context);
	    String registrationId = prefs.getString(PROPERTY_REG_ID, "");
	    mPhoneNumber = prefs.getString(PROPERTY_PHONE_NUMBER, "");
	    if (registrationId.isEmpty()) {
	        Log.i(TAG, "Registration not found.");
	        return "";
	    }
	    // Check if app was updated; if so, it must clear the registration ID
	    // since the existing regID is not guaranteed to work with the new
	    // app version.
	    int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
	    int currentVersion = getAppVersion(context);
	    if (registeredVersion != currentVersion) {
	        Log.i(TAG, "App version changed.");
	        return "";
	    }
	    return registrationId;
	}
	
	/**
     * Stores the registration ID and the app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @param context application's context.
     * @param regId registration ID
     */
	private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGCMPreferences(context);
        int appVersion = getAppVersion(context);
        Log.i(TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putString(PROPERTY_PHONE_NUMBER, mPhoneNumber);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

	/**
	 * @return Application's {@code SharedPreferences}.
	 */
	private SharedPreferences getGCMPreferences(Context context) {
	    // This sample app persists the registration ID in shared preferences, but
	    // how you store the regID in your app is up to you.
	    return getSharedPreferences(MainActivity.class.getSimpleName(),
	            Context.MODE_PRIVATE);
	}
	
	/**
	 * @return Application's version code from the {@code PackageManager}.
	 */
	private static int getAppVersion(Context context) {
	    try {
	        PackageInfo packageInfo = context.getPackageManager()
	                .getPackageInfo(context.getPackageName(), 0);
	        return packageInfo.versionCode;
	    } catch (NameNotFoundException e) {
	        // should never happen
	        throw new RuntimeException("Could not get package name: " + e);
	    }
	}
	
	/**
     * Registers the application with GCM servers asynchronously.
     * <p>
     * Stores the registration ID and the app versionCode in the application's
     * shared preferences.
     */
    private void registerInBackground() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    regid = gcm.register(SENDER_ID);
                    msg = "Device registered, registration ID=" + regid;

                    // Send the registration ID to server over HTTP
                    sendRegistrationIdToBackend();

                    // Persist the regID - no need to register again.
                    storeRegistrationId(context, regid);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                Log.i(TAG, msg);
                
                // Open ChatList activity
				chatListActivity();
				finish();
            }
        }.execute(null, null, null);
    }
    
    /**
     * Sends the registration ID to server over HTTP
     */
    private void sendRegistrationIdToBackend() {
    	HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(MainActivity.SERVER_ADDRESS + "android/users/add/");
        EditText phone_ = (EditText) findViewById(R.id.phone);
        mPhoneNumber = phone_.getText().toString();
        
        try {
            // Add your data
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
            nameValuePairs.add(new BasicNameValuePair("regid", regid));
            nameValuePairs.add(new BasicNameValuePair("phone", mPhoneNumber));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            // Execute HTTP Post Request
            HttpResponse response = httpclient.execute(httppost);
            
        } catch (ClientProtocolException e) {
        } catch (IOException e) {
        }
    }
}
